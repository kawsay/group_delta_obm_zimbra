# frozen_string_literal: true

module Delta
  module OBM
    class Group
      def self.all
        groups = from_csv

        puts "[-]\tMerging " + "`zimbra_slug`".bold + " to each entries"

        groups.each do |group|
          group.merge!(
            zimbra_slug: "groupe_#{group[:name]&.downcase&.gsub(' ', '_')&.delete('.')}@sdis21.org"
          )
        end
      end

      private

      def self.from_csv
        puts "[-] Reading CSV file: " + "resources/groups.csv".light_magenta.underline

        csv = CSV.readlines 'resources/groups.csv', col_sep: ';'
        
        groups = csv[1..-1].map do |line|
          {
            name: line[0],
            mail: line[9]
          }
        end

        puts "[>]".yellow + "\t#{groups.size}".light_yellow + " OBM groups processed"

        groups
      end
    end
  end
end
