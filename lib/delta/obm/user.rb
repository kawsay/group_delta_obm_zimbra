# frozen_string_literal: true

module Delta
  module OBM
    class User
      def self.all
        users = from_csv

        puts "[-]\tMerging " + "`has_employee_id?`".bold + " to each entries"

        users.each do |user|
          user.merge!(
            has_employee_id?: Delta::LDAP.has_employee_id?(sam_account_name: user[:id])
          )
        end

        puts "[>]".yellow + "\t#{users.size}".light_yellow + " OBM user accounts processed"

        users
      end

      def self.technical_accounts
        users = all

        puts "[-] Filtering OBM accounts which " + "`has_employee_id?`".bold + " equals " + "`false`".bold

        technical_accounts = users.select do |user|
          user[:has_employee_id?] == false
        end

        puts "[>]".yellow + "\t#{technical_accounts.size}".light_yellow + " technical accounts from OBM after filtering"

        technical_accounts
      end

      private

      def self.from_csv
        puts "[-] Reading CSV file: " + "resources/users.csv".light_magenta.underline

        csv = CSV.readlines 'resources/users.csv', col_sep: ';'

        csv[1..-1].map do |line|
          {
            id:        line[0],
            firstname: line[7],
            lastname:  line[6],
            mail:      line[18]
          }
        end
      end
    end
  end
end
