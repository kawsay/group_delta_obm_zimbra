# frozen_string_literal: true

require 'nokogiri'

module Delta
  module Zimbra
    class User
      def self.all
        puts '[-] Parsing HTML file: ' + 'resources/zimbra_users.html'.light_magenta.underline

        doc = Nokogiri::HTML(File.read('resources/zimbra_users.html'))

        users_div = doc.xpath '//div[contains(@class, "Row")]'

        users = users_div.map do |div|
          {
            mail: div.xpath('.//td[2]').text,
            fullname: div.xpath('.//td[3]').text
          }
        end

        puts "[>]".yellow + "\t#{users.size}".light_yellow + " Zimbra accounts processed"

        users
      end
    end
  end
end
