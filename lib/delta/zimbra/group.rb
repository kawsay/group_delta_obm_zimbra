# frozen_string_literal: true

module Delta
  module Zimbra
    class Group
      def self.all
        puts '[-] Parsing HTML file: ' + 'resources/zimbra_groups.html'.light_magenta.underline

        doc = Nokogiri::HTML(File.read('resources/zimbra_groups.html'))

        groups_div = doc.xpath '//div[contains(@class, "Row ")]'

        groups = groups_div.map do |div|
          {
            mail:     div.xpath('.//td[2]').text.strip,
            fullname: div.xpath('.//td[3]').text.strip
          }
        end

        puts "[>]".yellow + "\t#{groups.size}".light_yellow + " Zimbra groups processed"

        groups
      end
    end
  end
end
