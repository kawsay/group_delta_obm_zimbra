# frozen_string_literal: true

module Delta
  module Group
    def self.delta
      display_banner

      obm_groups    = Delta::OBM::Group.all
      zimbra_groups = Delta::Zimbra::Group.all

      group_delta = compute_group_delta(
        obm_groups:    obm_groups,
        zimbra_groups: zimbra_groups
      )

      write_delta_to_file group_delta: group_delta
      display_group_delta group_delta: group_delta
    end

    def self.existing_groups(groups: [])
      obm_groups    = Delta::OBM::Group.all
      zimbra_groups = Delta::Zimbra::Group.all

      groups = compute_existing_groups(
        obm_groups:    obm_groups,
        zimbra_groups: zimbra_groups
      )

      write_existing_groups_to_file groups: groups
    end

    private

    def self.write_existing_groups_to_file(groups: [])
      File.open('resources/output/existing_groups.txt', 'wb') do |f|
        groups.each do |d|
          f.puts(d[:name])
        end
      end

      puts "[+]".green + " Groups that exist in both OBM and Zimbra written in " + "resources/output/existing_groups.txt".light_magenta.underline
    end

    def self.write_delta_to_file(group_delta: [])
      File.open('resources/output/group_delta.txt', 'wb') do |f|
        group_delta.each do |d|
          f.puts(d[:name])
        end
      end
    end

    def self.display_banner
      puts
      puts "=".light_blue * 60
      puts " " * 25 + "GROUP DELTA".bold
      puts "=".light_blue * 60
      puts
    end

    def self.compute_group_delta(obm_groups: [], zimbra_groups: [])
      puts '[-] Computing the delta between OBM groups and Zimbra groups'

      zimbra_groups_arr = zimbra_groups.map { |group| group[:mail] }

      delta = obm_groups.select do |obm_group|
        !zimbra_groups_arr.include? obm_group[:zimbra_slug]
      end

      puts "[!]".red + " #{delta.size}".light_red + " OBM groups not present in Zimbra"

      delta
    end

    def self.compute_existing_groups(obm_groups: [], zimbra_groups: [])
      puts "[-] Computing groups that exists in both OBM and Zimbra"

      zimbra_groups_arr = zimbra_groups.map { |group| group[:mail] }

      exist = obm_groups.select do |obm_group|
        zimbra_groups_arr.include? obm_group[:zimbra_slug]
      end
    end

    def self.display_group_delta(group_delta: [])
      header = "\t" +
        "Name".ljust(50) +
        "Mail".ljust(40) +
        "Zimbra slug"

      puts header.bold

      group_delta.each do |group|
        next if group[:name].nil?

        puts "\t" +
          group[:name].ljust(50) +
          group[:mail].ljust(40) +
          group[:zimbra_slug]
      end
    end
  end
end
