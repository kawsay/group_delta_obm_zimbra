# frozen_string_literal: true

module Delta
  module User
    def self.delta
      display_banner

      obm_tech_accounts = Delta::OBM::User.technical_accounts
      zimbra_users      = Delta::Zimbra::User.all

      account_delta = compute_account_delta(
        obm_tech_accounts: obm_tech_accounts,
        zimbra_users:      zimbra_users
      )

      display_account_delta delta: account_delta
    end

    private

    # def self.write_delta_to_file(delta: [])
    #   File.open('resources/output/group_delta.txt', 'wb') do |f|
    #     delta.each do |d|
    #       f.puts(d[:name])
    #     end
    #   end
    # end

    def self.display_banner
      puts
      puts "=".light_blue * 60
      puts " " * 25 + "USER DELTA".bold
      puts "=".light_blue * 60
      puts
    end

    def self.compute_account_delta(obm_tech_accounts: [], zimbra_users: [])
      puts '[+] Computing the delta between OBM technical accounts and Zimbra accounts'

      zimbra_users_arr = zimbra_users.map { |user| user[:mail] }

      delta = obm_tech_accounts.select do |obm_account|
        !zimbra_users_arr.include? obm_account[:mail]
      end

      puts "[!]".red + " #{delta.size}".light_red + " OBM technical accounts not present in Zimbra"

      delta
    end

    def self.display_account_delta(delta: [])
      header = "\t" +
        "ID".ljust(30) +
        "Firstname".ljust(30) +
        "Lastname".ljust(30) +
        "Mail".ljust(40) +
        "employeeID"

      puts header.bold

      delta.each do |account|
        puts "\t" +
          account[:id].ljust(30) +
          account[:firstname].ljust(30) +
          account[:lastname].ljust(30) +
          account[:mail].ljust(40) +
          account[:has_employee_id?].to_s
      end
    end
  end
end
