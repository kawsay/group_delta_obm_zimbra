# frozen_string_literal: true

require_relative './obm/group'
require_relative './obm/user'

module Delta
  module OBM
    def self.all
      {
        users:  Delta::OBM::User.all,
        groups: Delta::OBM::Group.all
      }
    end
  end
end
