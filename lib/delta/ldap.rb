# frozen_string_liberal: true

require 'net/ldap'

module Delta
  class LDAP
    def self.has_employee_id?(sam_account_name: '')
      ldap_query do
        filter = Net::LDAP::Filter.eq 'samAccountName', sam_account_name

        entry = ldap.search(
          base:      ENV['LDAP_BASE'],
          filter:    filter,
          attribute: 'employeeID'
        )

        return nil if entry.empty?

        entry[0][:employeeid].empty? ? false : true
      end
    end

    private

    def self.ldap_query(&block)
      if ldap.bind
        yield block if block_given?
      end
    end

    def self.ldap
      return @@ldap if defined?(@@ldap)

      puts "[+]".green + "\tLDAP bind " + "successful".light_green

      @@ldap = Net::LDAP.new

      @@ldap.host = ENV['LDAP_HOST']
      @@ldap.port = ENV['LDAP_PORT']
      @@ldap.auth ENV['LDAP_USERNAME'], ENV['LDAP_PASSWORD']

      @@ldap
    end
  end
end
