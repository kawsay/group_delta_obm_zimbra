# frozen_string_liberal: true

require 'colorize'

puts '[*]'.light_cyan + ' Loading external libraries'

require 'csv'
require 'json'
require 'dotenv/load'
require 'pry'
require_relative './delta/user'
require_relative './delta/group'
require_relative './delta/ldap'
require_relative './delta/obm'
require_relative './delta/zimbra'

module Delta
  def self.run
    # binding.pry

    Delta::User.delta
    Delta::Group.delta

    # Delta::Group.existing_groups
  end

end

Delta.run

